<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<title>The Hudson URL shortener</title>
		{!! HTML::style('/css/app.css') !!}
	</head>
	<body>
		
		<div id="holder" class="{{ $area }}">
@yield('content')

		</div>
		
	</body>
</html>