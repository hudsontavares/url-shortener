@extends('app', ["area" => "home"])

@section('content')
	
@if (isset($message))
	<div id="message">{{$message}}</div>
@endif
	
			<h1>Welcome to the <strong>Hudson</strong> URL shortener</h1>
	
	
		
			{!! Form::open(["route" => "shorturl.save", "method" => "post"]) !!}
				
				{!! Form::label('full_address', 'Please, enter the URL you want to shorten:') !!}
				{!! Form::text('full_address') !!}
				{!! Form::submit('Create') !!}
			
			{!! Form::close() !!}
	
	
@if (count($latest) != 0)
			<h2>Latest entries ({{ count($latest) }})</h2>
			<ul id="last-entries">
				@foreach($latest as $item)
					<li>
						{!! HTML::link($item->short_name, $item->title . ($item->total_clicks > 0 ? sprintf(' (clicks: %s)', $item->total_clicks) : ''), ['target' => '_blank']) !!}
						<p>{{$item->description}}</p>
					</li>
				@endforeach
		
			</ul>
@endif
	
@endsection

