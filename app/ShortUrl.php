<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortUrl extends Model{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['full_address', 'title', 'description', 'short_name'];
        protected $appends = ['total_clicks'];
        
        public function scopeLastest($query)
        {
            return $query->orderBy('created_at', 'DESC')->limit(20);
        }
        
        public function getTotalClicksAttribute()
        {
            return $this->hasMany('\App\Click', 'shorturl_id', 'id')->count();
        }
        
        /**
         *  Generates a random string for the short_name field
         *
         *  @return string the random string
         */
        static function getShortName($full_address){
            
            $shortName = '';
            $length    = 4;
            
            $exists = static::whereFullAddress($full_address)->first();
            if(count($exists) > 0)return $exists->short_name;
            
            do
            {
                $last = static::whereRaw('LENGTH(short_name) = ?', [$length])->orderBy('short_name', 'DESC')->first();
                
                if(is_null($last))
                {
                    $shortName = str_repeat('a', $length);
                    break;
                }
                
                $nextLetter = ord(substr($last->short_name, -1));
                if($nextLetter == 122)$nextLetter = 47;
                
                
                if(preg_match('/[a-z0-9]/iu', chr(++$nextLetter)) === 0)
                {
                    
                    $length++;
                    continue;
                }
                
                $shortName = substr($last->short_name, 0, strlen($last->short_name) - 1) . chr($nextLetter);
                
            }while(static::whereShortName($shortName)->count() > 0);
            
            return $shortName;
        }

}
