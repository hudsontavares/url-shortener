<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Click extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['ip_address', 'shorturl_id'];
}
