<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/{short_name}', ['as' => "shorturl.forward", 'uses' => "ShortUrlController@forward"])->where(['short_name' => "[a-zA-Z0-9]{4,}"]);
Route::post('/str', ['as' => "shorturl.save", 'uses' => 'ShortUrlController@store']);
