<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Session;
use \App\ShortUrl;
use \App\Click;

class ShortUrlController extends Controller {

	/**
	 * Stores a new URL into the database
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
            $full_address = $request->input('full_address');
            $returnData   = ['status' => true, 'message' => 'The registry has been added successfully.'];
            
            try
            {
                /* We get and parse the URL  */
                if($full_address == '' || ($parsed = parse_url($full_address)) === false)
                    throw new \Exception(sprintf('Unable to parse the URL "%s".', $full_address));
                
                if(strpos($full_address, 'http://') === false && strpos($full_address, 'https://') === false)
                    throw new \Exception('This tool can only shorten URIs starting from HTTP and HTTPS protocols.');
                
                $data   = @file_get_contents($full_address, null, stream_context_create(['http' => ['timeout' => 5]]), -1, 32768);
                $dom    = new \DOMDocument('1.0', 'UTF-8');
                
                if($data === false || @$dom->loadHTML('<?xml version="1.0" charset="UTF-8" ?>' . $data) === false)
                    throw new \Exception(sprintf('Unable to read the URI "%s". The address is incorrect or the server is down.', $full_address));
                
                $title = $dom->getElementsByTagName('title');
                $title = $title->length != 0 ? $title->item(0)->nodeValue : $full_address;
                
                foreach($dom->getElementsByTagName('meta') as $value)
                    if(strtolower($value->getAttribute('name')) == 'description')
                        $description = $value->getAttribute('content');
                        
                if(!isset($description) || is_null($description))$description = 'No description found.';
                
                $current = ShortUrl::firstOrNew(['short_name' => ShortUrl::getShortName($full_address)]);
                
                $current->title         = $title;
                $current->description   = $description;
                $current->full_address  = $full_address;
                $current->short_name    = ShortUrl::getShortName($full_address);
                
                $current->save();
            }
            catch(\Exception $e)
            {
                $returnData['status']   = false;
                $returnData['message']  = $e->getMessage();
            }
            
            $latest = ShortUrl::latest()->get();
            Session::flash('message', $returnData['message']);
            
            return redirect('/');
	}
        
        public function forward(Request $request, $short_name)
        {
            try
            {
                $current = ShortUrl::where('short_name', '=', $short_name)->first();
                
                if(is_null($current))
                    throw new \Exception('Short URL not found.');
                
                Click::create(['ip_address' => $request->ip(), 'shorturl_id' => $current->id]);
                $returnData = ['status' => true, 'message' => 'The URL has been found', 'url' => $current->full_address];
            }
            catch(\Exception $e)
            {
                $returnData = ['status' => false, 'message' => $e->getMessage(), 'url' => '/'];
            }
            
            return redirect($returnData['url']);
        }

}
