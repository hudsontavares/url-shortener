<?php namespace App\Http\Controllers;

use \App\ShortUrl;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller {

	/**
	 * Show the application main screen to the user
	 *
	 * @return Response
	 */
	public function index()
	{
		$latest = ShortUrl::lastest()->get();
		$message= Session::get('message');
		
		return view('home', compact('latest', 'message'));
	}

}
