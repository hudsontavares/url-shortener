Hudson's URL Shortener

It's a Laravel 5 app that receives and shortens an specified URL. You can see it alive
at the address http://www.nhsistemas.com/mindvalley/urlshortener/public/