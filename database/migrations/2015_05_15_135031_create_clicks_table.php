<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClicksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('clicks'))Schema::drop('clicks');
		
		Schema::create('clicks', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->integer('shorturl_id')->unsigned();
			$table->string('ip_address', 255);
			
			$table->timestamps();
			$table->foreign('shorturl_id')->references('id')->on('short_urls')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('clicks'))Schema::drop('clicks');
	}

}
