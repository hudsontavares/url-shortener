<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShortUrlsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('short_urls'))Schema::drop('short_urls');
		Schema::create('short_urls', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->string('full_address', 255);
			$table->string('short_name', 255);
			$table->string('title', 255);
			$table->text('description');
			
			$table->unique('short_name');
			$table->unique('full_address');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('short_urls'))Schema::drop('short_urls');
	}

}
